package reflectionAnnotations;

public class Entity {
  @Secured(intValue = 33)
  private void metodOnePrivate() {}

  @Secured(intValue = 33, stringValue = "anotation")
  private void metodTwoPrivate() {}

  public void metodOnePublic() {}
}
