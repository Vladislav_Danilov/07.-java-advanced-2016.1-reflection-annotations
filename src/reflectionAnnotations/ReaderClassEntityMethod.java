package reflectionAnnotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class ReaderClassEntityMethod {
  public String readClassEntityMethod(String metodName) {
    Class<?> c = null;
    try {
      c = Class.forName("reflectionAnnotations.Entity");
    } catch (ClassNotFoundException e1) {
      e1.printStackTrace();
    }
    Method method = null;
    try {
      method = c.getDeclaredMethod(metodName);
      method.setAccessible(true);
      System.out.println();
    } catch (NoSuchMethodException | SecurityException e) {
      e.printStackTrace();
    }
    Annotation annotations = method.getAnnotation(Secured.class);
    Secured secured = null;
    if (annotations instanceof Secured) {
      secured = (Secured) annotations;
    }
    if (secured != null) {
      return "method = " + method.toString() + " intValue: " + secured.intValue() + "stringValue: "
          + secured.stringValue();
    } else {
      return "method = " + method.toString();
    }
  }
}
