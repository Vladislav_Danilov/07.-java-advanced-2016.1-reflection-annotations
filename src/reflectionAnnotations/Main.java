package reflectionAnnotations;

public class Main {
  public static void main(String[] args) {
    ReaderClassEntityMethod readerClassEntityMethod = new ReaderClassEntityMethod();
    System.out.println(readerClassEntityMethod.readClassEntityMethod("metodOnePrivate"));
    System.out.println(readerClassEntityMethod.readClassEntityMethod("metodTwoPrivate"));
    System.out.println(readerClassEntityMethod.readClassEntityMethod("metodOnePublic"));
  }
}
