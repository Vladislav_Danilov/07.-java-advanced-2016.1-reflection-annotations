package reflectionAnnotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)

public @interface Secured {
  int intValue();
  String stringValue() default "strict";
}
